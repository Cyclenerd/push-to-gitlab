# Push to GitLab Repository

Modify GitLab repositories from GitLab CI pipelines...

You can push to a GitLab repo with [Deploy Key](#deploy-key) or [Project Access Token](#project-access-token).

Working GitLab CI configuration [.gitlab-ci.yml](https://gitlab.com/Cyclenerd/push-to-gitlab/-/blob/main/.gitlab-ci.yml)

## Deploy Key

- [ ] Generate a new SSH key pair:
	```shell
	ssh-keygen -t ed25519 -C "gitlab" -f "gitlab"
	```
	New files:
	* Private key: `gitlab`
	* Public key: `gitlab.pub`
- [ ] Add public key as deploy key with write access (left sidebar, select Settings » Repository than "Deploy keys")
	* Select "Grant write permissions to this key" (Allow this key to push to this repository)
- [ ] Add private key as variable `GIT_PUSH_KEY` (left sidebar, select Settings » CI/CD than "Variables")
- [ ] Use the token to push your changes

```shell
git config user.email "gitlab-runner@gitlab.com"
git config user.name "gitlab-runner[bot]"
git checkout -b main
# Prepare git for pushing back to repository
mkdir -pvm 0700 .ssh
echo "$GIT_PUSH_KEY" > .ssh/gitlab
chmod 0400 .ssh/gitlab
export GIT_SSH_COMMAND="ssh -o StrictHostKeyChecking=no -i .ssh/gitlab"
# Push
git push -o ci.skip git@gitlab.com:Cyclenerd/push-to-gitlab.git main
```

## Project Access Token

> **Note** Premium license tier or higher!

- [ ] Create a project access token with `write_repository` access (left sidebar, select Settings » Access Tokens)
- [ ] Add the token as a masked variable `GIT_PUSH_TOKEN` (left sidebar, select Settings » CI/CD than "Variables")
- [ ] Modify the repository, preferably in a separate branch
    - Main/master branch is protected by default. You can remove the protection (left sidebar, select Settings » Repository than "Protected branches") or use another branch.
- [ ] Use the token to push your changes

```shell
git config user.email "gitlab-runner@gitlab.com"
git config user.name "gitlab-runner[bot]"
git checkout -b main
git push -o ci.skip "https://gitlab-runner:${GIT_PUSH_TOKEN}@${CI_REPOSITORY_URL#*@}" main
```

## Skip

Add `-o ci.skip` to `git push` to tell the server to not trigger any jobs with this change.
We do not want to start an infinite series of jobs.
